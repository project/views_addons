<?php

/**
 * @file
 * Using the Views API to create a new field.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_addons_views_data_alter(array &$data) {
  $data['views']['views_addons_custom_advanced'] = [
    'title' => t('Advanced Custom Text'),
    'field' => [
      'title' => t('Advanced Custom Text'),
      'help' => t('Custom Text with altered filtering of Tags.'),
      'id' => 'views_addons_custom_advanced',
    ],
  ];
  $data['views']['views_addons_add_entity'] = [
    'title' => t('Add Entity Link'),
    'help' => t('Adds a customizable link to add a new entity.'),
    'area' => [
      'id' => 'views_addons_add_entity',
    ],
  ];
}
