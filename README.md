# Views Addons
Additional features for Drupal views.

## Advanced Custom Text
A new field that can be used in views like the Custom Text field, but
without the limitations of the core field. Additional tags can be allowed in
this field in the configuration. This makes svg, picture, source tags and so on
usable which get filtered in the normal Custom Text field provided in the Drupal
core.
